package com.test.junit.mockito.junit.mockito.test.component;

import com.test.junit.mockito.junit.mockito.test.entity.PersonEntity;
import com.test.junit.mockito.junit.mockito.test.model.PersonModel;
import com.test.junit.mockito.junit.mockito.test.properties.ApplicationProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UtilComponentTest {

  @InjectMocks
  private UtilComponent utilComponent;

  @Mock
  private ApplicationProperties applicationProperties;

  private PersonEntity personEntity;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);

    personEntity = new PersonEntity();
    personEntity.setPersonId(4);
    personEntity.setName("Daniel");
    personEntity.setLastName("Avila");
    personEntity.setBirthDate("11/11/1995");
  }

  @Test
  public void mapperTest1() {
    personEntity.setStatus("00");

    Mockito.when(applicationProperties.getPostulanteReason()).thenReturn("Postulante");

    PersonModel per = utilComponent.mapper(personEntity);

    Assertions.assertEquals(4, per.getPersonId());
    Assertions.assertEquals("Postulante", per.getStatus());
  }

  @Test
  public void mapperTest2() {

    personEntity.setStatus("01");

    Mockito.when(applicationProperties.getInscritoReason()).thenReturn("Inscrito");

    PersonModel per = utilComponent.mapper(personEntity);

    Assertions.assertEquals(4, per.getPersonId());
    Assertions.assertEquals("Inscrito", per.getStatus());
  }

  @Test
  public void mapperTest3() {

    personEntity.setStatus("02");

    Mockito.when(applicationProperties.getEnCursoReason()).thenReturn("En Curso");

    PersonModel per = utilComponent.mapper(personEntity);

    Assertions.assertEquals(4, per.getPersonId());
    Assertions.assertEquals("En Curso", per.getStatus());
  }

  @Test
  public void mapperTest4() {

    personEntity.setStatus("03");

    Mockito.when(applicationProperties.getBajaTemporalReason()).thenReturn("Baja Temporal");

    PersonModel per = utilComponent.mapper(personEntity);

    Assertions.assertEquals(4, per.getPersonId());
    Assertions.assertEquals("Baja Temporal", per.getStatus());
  }

  @Test
  public void mapperTest5() {

    personEntity.setStatus("04");

    Mockito.when(applicationProperties.getBajaDefinitivaReason()).thenReturn("Baja Definitiva");

    PersonModel per = utilComponent.mapper(personEntity);

    Assertions.assertEquals(4, per.getPersonId());
    Assertions.assertEquals("Baja Definitiva", per.getStatus());
  }

  @Test
  public void mapperTest6() {

    personEntity.setStatus("-1");

    Mockito.when(applicationProperties.getDefaultReason()).thenReturn("Default");

    PersonModel per = utilComponent.mapper(personEntity);

    Assertions.assertEquals(4, per.getPersonId());
  }

  // @Test
  // public void mapperTest7() {
  //
  // Mockito.when(applicationProperties
  // .valor(Mockito.anyInt()))
  // .thenReturn("Alpha");
  //
  // personEntity.setPersonId(3);
  // personEntity.setStatus("GE");
  //
  // utilComponent.mapper(personEntity);
  // }
}
