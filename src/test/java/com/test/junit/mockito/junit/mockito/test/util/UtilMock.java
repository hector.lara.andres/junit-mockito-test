package com.test.junit.mockito.junit.mockito.test.util;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
public class UtilMock {
    
  @Mock
  private List<String> listMock;
  
  private List<String> listSpy = Mockito.spy(List.class);
  
  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }
  
  @Test
  public void mocks() {
    listMock.add("Torre");
    listMock.add("Peón");
    listMock.add("Alfil");
    listMock.add("Dama");
    
    Mockito.verify(listMock).add("Torre");
    Mockito.verify(listMock).add("Peón");
    Mockito.verify(listMock).add("Alfil");
    
    System.out.println("Número de elementos Mock: " + listMock.size());
  }
  
  @Test
  public void spys() {
    listSpy.add("Torre");
    listSpy.add("Peón");
    listSpy.add("Alfil");
    
    Mockito.verify(listSpy).add("Torre");
    Mockito.verify(listSpy).add("Peón");
    Mockito.verify(listSpy).add("Alfil");
    
    System.out.println("Número de elementos Spy: " + listSpy.size());
  }

}
