package com.test.junit.mockito.junit.mockito.test.model;

import lombok.Data;

@Data
public class PersonModel {
  
  private int personId;
  
  private String name;
  
  private String lastName;
  
  private String date;
  
  private String status;
  
}
